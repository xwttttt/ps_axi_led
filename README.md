1.  IP集成开发，创建一个ZYNQ7 Processing System，配置串口，Bank1选择1.8V，打开48、49所对应的UART，DDR接口设置为芯片所对应的接口，时钟设置为767MHz，完成IP核的配置。
2.  创建axi_gpio的IP核，设置All Outputs，输出宽度设为4（分别控制4个LED灯），重命名gpio为leds，自动连线，优化布局，可以验证一下设计是否正确，保存。
3.  生成HDL包装，生成一个.v文件，Generate Output Products，要对led分配管脚，添加约束led.xdc。
4.  综合实现生成比特流文件。
5.  导出硬件，运行SDK，创建名称为axi_led的工程，模板选择Hello World，导入gpio的例程，将比特流文件写入到FPGA中，将LED定义为0x0F，执行程序，观察到LED4个灯快速闪烁。